const sgMail = require('@sendgrid/mail')
//const sendgradAPIKey = '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'

sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const sendWelcomeEmail = (email, name) => {
    sgMail.send({
        to: email,
        from: 'shiplu.hossen31963196@gmail.com',
        subject: 'Thanks for joining in!',
        text: 'Welcome to the app,' + name + ', let me know how you get along with the app.'
    })
}

const sendDeletedEmail = (email, name) => {
    sgMail.send({
        to: email,
        from: 'shiplu.hossen31963196@gmail.com',
        subject: 'Cancel Your Account',
        text: name + ', Your account have permanentlt deleted from our App'
    })
}

module.exports = {
    sendWelcomeEmail,
    sendDeletedEmail
}