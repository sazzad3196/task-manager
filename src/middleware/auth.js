const jwt = require('jsonwebtoken')
const User = require('../models/user')

const Auth = async (req, res, next) => {
    const token = req.header('Authorization').replace('Bearer ', '')
    console.log('Token: ' + token)
    try {
        const decode = jwt.verify(token, process.env.JWT_SECRTE)
        const user = await User.findOne({_id: decode._id, 'tokens.token': token})
        if(!user) {
            throw new Error()
        }

        req.token = token
        req.user = user
        next()
    }catch(e) {
        res.status(404).send({error: 'Please authenticate'})
    }
}

module.exports = Auth