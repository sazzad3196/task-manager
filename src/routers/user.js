const express = require('express')
const multer = require('multer')
const sharp = require('sharp')
const router = new express.Router()
const User = require('../models/user')
const Auth = require('../middleware/auth')
const { sendWelcomeEmail, sendDeletedEmail} = require('../emails/account')

router.post('/users', async (req, res) => {
    const user = new User(req.body)
    try {
        await user.save()
        sendWelcomeEmail(user.email, user.name)
        const token = await user.generatedAuthToken()
        res.status(201).send({ user, token})
    }catch(e) {
        res.status(400).send(e)
    }
})

router.get('/users', async (req, res) => {
    try {
        const users = await User.find({})
        res.status(201).send(users)
    }catch(e) {
        res.status(404).send(e)
    }
})

router.get('/users/me', Auth, (req, res) => {
    res.send(req.user)
})
 
router.get('/user/:id', async (req, res) => {
    const _id = req.params.id
    try {
        const user = await User.findById(_id)
        res.send(user)
    }catch(e) {
        res.send(e)
    }
})

// router.patch('/users/:id', async (req, res) => {
//     const updates = Object.keys(req.body)
//     const allowUpdates = ['name', 'email', 'password', 'age']
//     const isValidOperation = updates.every((update) => allowUpdates.includes(update))
//     if(!isValidOperation) {
//         res.status(400).send({error: 'Invalid update'})
//     }
//     else {
//         try {
//             //const user = await User.findByIdAndUpdate(req.params.id, req.body, {new: true, runValidators: true})
//             const user = await User.findById(req.params.id)
//             updates.forEach((update) => user[update] = req.body[update])
//             await user.save()

//             if(!user) {
//                 return res.status(404).send()
//             }
//             res.send(user)
//         }catch(e) {
//             res.status(400).send(e)
//         }
//     }

// })

// router.delete('/users/:id', async (req, res) => {
//     try {
//         const user = await User.findByIdAndRemove(req.params.id)
//         if(!user) {
//             return res.status(400).send()
//         }
//         res.status(200).send(user)
//     }catch(e) {
//         res.status(404).send(e)
//     }
// })

router.delete('/users/me', Auth, async (req, res) => {
    try {
        await req.user.remove()
        sendDeletedEmail(req.user.email, req.user.name)
        res.status(200).send(req.user)
    }catch(e) {
        res.status(404).send(e)
    }
})

router.patch('/users/me', Auth, async (req, res) => {
    const updates = Object.keys(req.body)
    const allowUpdates = ['name', 'email', 'password', 'age']
    const isValidOperation = updates.every((update) => allowUpdates.includes(update))
    if(!isValidOperation) {
        res.status(400).send({error: 'Invalid update'})
    }
    else {
        try {
            updates.forEach((update) => req.user[update] = req.body[update])
            await req.user.save()
            
            res.send(req.user)
        }catch(e) {
            res.status(400).send(e)
        }
    }

})

router.post('/users/login', async(req, res) => {
    try{
        const user = await User.findByCredentials(req.body.email, req.body.password)
        const token = await user.generatedAuthToken()
        //res.send({ user: user.getPublicProfile(), token})
        res.send({ user: user, token})
    }catch(e) {
        res.status(404).send(e)
    }
})

router.post('/users/signup', async(req, res) => {
    const user = new User(req.body)
    try{
        const token = await user.generatedAuthToken()
        res.send({ user, token})
    }catch(e) {
        res.status(404).send(e)
    }
})

router.post('/users/logout', Auth, async(req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token !== req.token
        })
        await req.user.save()
        res.send()
    }catch(e) {
        res.status(500).send()
    }
})

router.post('/users/logoutAll', Auth, async(req, res) => {
    try {
        req.user.tokens = []
        await req.user.save()
        res.send()
    }catch(e) {
        res.status(500).send()
    }
})

const upload = multer({
    limits: {
        fileSize: 1000000
    },
    fileFilter(req, file, cb) {
        if(!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
            return cb(new Error('Upoad file must be jpg or jpeg or png'))
        }
        cb(undefined, true)
    }
})

router.post('/users/me/avatar', Auth, upload.single('avatar'), async (req, res) => {
    const buffer = await sharp(req.file.buffer).resize({height: 250, width: 250}).png().toBuffer()
    req.user.avatar = buffer
    await req.user.save()
    res.send()
}, (error, req, res, next) => {
    res.status(400).send({ error: error.message })
})

router.delete('/users/me/avatar', Auth, async (req, res) => {
    try {
        req.user.avatar = undefined
        await req.user.save()
        res.status(200).send()
    }catch(e) {
        res.status(404).send(e)
    }
    
})

router.get('/users/:id/avatar', async(req, res) => {
    try {
        const user = await User.findById(req.params.id)
        if(!user || !user.avatar) {
            throw new Error()
        }

        res.set('Content-Type', 'image/png ')
        res.send(user.avatar)
    }catch(e) {
        res.status(400).send(e)
    }
})

module.exports = router