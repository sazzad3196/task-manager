// CRUD Insert Read Update Delete

// const mongodb = require('mongodb')
// const mongoClient = mongodb.MongoClient

const {MongoClient, ObjectID } = require('mongodb')

const connectionURL = 'mongodb://localhost:27017'
const databaseName = 'task-manager'

const id = new ObjectID()

MongoClient.connect(connectionURL, {useNewUrlParser: true}, (error, client) => {
    if(error) {
        return console.log('Unable to connect to database')
    }
    
    console.log('Connection successfully.')
    const db = client.db(databaseName)

    // db.collection('users').insertOne({
    //     _id: new ObjectID(),
    //     name: 'Mamur beta',
    //     age: 24
    // }, (error, result) => {
    //     if(error) {
    //         return console.log('Unable to insert user.')
    //     }

    //     console.log(result.ops)
    // })

    // db.collection('users').insertMany([
    //     {
    //         name: 'pritam',
    //         age: 25
    //     },
    //     {
    //         name: 'joy',
    //         age: 28
    //     }
    // ], (error, result) => {
    //     if(error) {
    //         return console.log('Unable to insert user.')
    //     }
        
    //     console.log(result.ops)
    // })

    // db.collection('tasks').insertMany([
    //     {
    //         description: 'Clean the house',
    //         completed: true
    //     },
    //     {
    //         description: 'Renew inspection',
    //         completed: false
    //     }
    // ], (error, result) => {
    //     if(error) {
    //         return console.log('Unable to insert user.')
    //     }
        
    //     console.log(result.ops)
    // })

    // db.collection('users').find().toArray((error, tasks) => {
    //     if(error) {
    //         return console.log('Error happened!')
    //     }

    //     console.log(tasks)
    // })

    // db.collection('tasks').findOne({_id: new ObjectID("5f133f9cd2afab67ac12dc37")}, (error, result) => {
    //     if(error) {
    //         return console.log('Error happened!')
    //     }

    //     console.log(result)
    // })

    // db.collection('tasks').findOne({_id: new ObjectID("5f133f9cd2afab67ac12dc37"), description : "Renew inspection", completed : false}, (error, result) => {
    //     if(error) {
    //         console.log('Unable to fatch')
    //     }
    //     else if(result == null) {
    //         console.log('Document not found!')
    //     }
    //     else {
    //         console.log(result)
    //     }
        
    // })

    // const updatePromise = db.collection('users').updateOne(
    //     {
    //         _id: new ObjectID("5f135219910a645c6c63d22c")
    //     },
    //     {
    //         $set: {
    //             name: 'Jhony'
    //         }
    //     }
    // )

    // updatePromise.then((result) => {
    //     console.log(result)
    // }).catch((error) => {
    //     console.log(error)
    // })

    // const updatePromise = db.collection('tasks').updateMany(
    //     {
    //         completed: true,
    //         _id: new ObjectID("5f133f886e2d0e48042ab25c")
    //     },
    //     {
    //         $set: {
    //             description: "Cleaning everything quickly"
    //         }
    //     }
    // )

    // updatePromise.then((result) => {
    //     console.log(result)
    // }).catch((error) => {
    //     console.log(error)
    // })

    const deletePromise = db.collection('users').deleteOne(
        {
            age: 24
        }
    )

    deletePromise.then((result) => {
        console.log(result)
    }).catch((error) => {
        console.log(error)
    })

})